msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-26 03:35+0100\n"
"PO-Revision-Date: 2019-03-02 16:12-0800\n"
"Last-Translator: Japanese KDE translation team <kde-jp@kde.org>\n"
"Language-Team: Japanese <kde-jp@kde.org>\n"
"Language: ja\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Accelerator-Marker: &\n"
"X-Text-Markup: kde4\n"

#: ../../reference_manual/preferences/tablet_settings.rst:1
msgid "Configuring the tablet in Krita."
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:17
msgid "Tablet Settings"
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:20
msgid ".. image:: images/en/Krita_Preferences_Tablet_Settings.png"
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:21
msgid "Tablet"
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:22
msgid ""
"Input Pressure Global Curve : This is the global curve setting that your "
"tablet will use in Krita. The settings here will make your tablet feel soft "
"or hard globally."
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:24
msgid "on Windows 8 or above only"
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:26
msgid "WinTab"
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:27
msgid ""
"Use the WinTab API to receive tablet pen input. This is the API being used "
"before Krita 3.3. This option is recommended for most Wacom tablets."
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:29
msgid ""
"Use the Pointer Input messages to receive tablet pen input. This option "
"depends on Windows Ink support from the tablet driver. This is a relatively "
"new addition so it's still considered to be experimental, but it should work "
"well enough for painting. You should try this if you are using an N-Trig "
"device (e.g. recent Microsoft Surface devices) or if your tablet does not "
"work well with WinTab."
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:30
msgid "For Krita 3.3 or later:Tablet Input API"
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:30
msgid "Windows 8+ Pointer Input"
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:33
msgid "Tablet Tester"
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:37
msgid ""
"This is a special feature for debugging tablet input. When you click on it, "
"it will open a window with two sections. The left section is the **Drawing "
"Area** and the right is the **Text Output**."
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:39
msgid ""
"If you draw over the Drawing Area, you will see a line appear. If your "
"tablet is working it should be both a red and blue line."
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:41
msgid ""
"The red line represents mouse events. Mouse events are the most basic events "
"that Krita can pick up. However, mouse events have crude coordinates and "
"have no pressure sensitivity."
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:43
msgid ""
"The blue line represents the tablet events. The tablet events only show up "
"when Krita can access your tablet. These have more precise coordinates and "
"access to sensors like pressure sensitivity."
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:47
msgid ""
"If you have no blue line when drawing on the lefthand drawing area, Krita "
"cannot access your tablet. Check out the :ref:`page on drawing tablets "
"<drawing_tablets>` for suggestions on what is causing this."
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:49
msgid ""
"When you draw a line, the output on the right will show all sorts of text "
"output. This text output can be attached to a help request or a bug report "
"to figure out what is going on."
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:52
msgid "External Links"
msgstr ""

#: ../../reference_manual/preferences/tablet_settings.rst:54
msgid ""
"`David Revoy wrote an indepth guide on using this feature to maximum "
"advantage. <http://www.davidrevoy.com/article182/calibrating-wacom-stylus-"
"pressure-on-krita>`_"
msgstr ""
