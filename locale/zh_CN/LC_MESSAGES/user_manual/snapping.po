msgid ""
msgstr ""
"Project-Id-Version: kdeorg\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-10 03:45+0200\n"
"PO-Revision-Date: 2019-04-09 18:50\n"
"Last-Translator: guoyunhe <i@guoyunhe.me>\n"
"Language-Team: Chinese Simplified\n"
"Language: zh_CN\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=1; plural=0;\n"
"X-Generator: crowdin.com\n"
"X-Crowdin-Project: kdeorg\n"
"X-Crowdin-Language: zh-CN\n"
"X-Crowdin-File: /kf5-trunk/messages/www/"
"docs_krita_org_user_manual___snapping.pot\n"

#: ../../user_manual/snapping.rst:1
msgid "How to use the snapping functionality in Krita."
msgstr "介绍如何在 Krita 里面使用吸附功能。"

#: ../../user_manual/snapping.rst:15
msgid "Snapping"
msgstr "吸附"

#: ../../user_manual/snapping.rst:17
msgid ""
"In Krita 3.0, we now have functionality for Grids and Guides, but of course, "
"this functionality is by itself not that interesting without snapping."
msgstr ""
"从 Krita 3.0 开始我们提供了网格和参考线，但要是离开了吸附功能，网格和参考线就"
"会失去意义。"

#: ../../user_manual/snapping.rst:21
msgid ""
"Snapping is the ability to have Krita automatically align a selection or "
"shape to the grids and guides, document center and document edges. For "
"Vector layers, this goes even a step further, and we can let you snap to "
"bounding boxes, intersections, extrapolated lines and more."
msgstr ""
"吸附是 Krita 把选区或者形状自动对齐到网格和参考线、文档中心和边框等处的功能。"
"在矢量图层里面还可系吸附到对象的外框、交点、延长线等处。"

#: ../../user_manual/snapping.rst:26
msgid ""
"All of these can be toggled using the snap pop-up menu which is assigned to :"
"kbd:`Shift+S`."
msgstr "要切换各种吸附功能的开关状态，按 :kbd:`Shift+S` 即可弹出相应的菜单。"

#: ../../user_manual/snapping.rst:29
msgid "Now, let us go over what each option means:"
msgstr "接下来让我们介绍一下每种吸附功能的特性："

#: ../../user_manual/snapping.rst:32
msgid ""
"This will snap the cursor to the current grid, as configured in the grid "
"docker. This doesn’t need the grid to be visible. Grids are saved per "
"document, making this useful for aligning your art work to grids, as is the "
"case for game sprites and grid-based designs."
msgstr ""
"将光标吸附到当前网格，网格可在网格面板进行配置。隐藏网格后吸附依然生效。网格"
"配置会保存到文档中。用于将将画作对齐到网格，例如游戏的像素拼合图或者基于网格"
"的设计等。"

#: ../../user_manual/snapping.rst:34
msgid "Grids"
msgstr "网格"

#: ../../user_manual/snapping.rst:37
msgid ""
"This allows you to snap to guides, which can be dragged out from the ruler. "
"Guides do not need to be visible for this, and are saved per document. This "
"is useful for comic panels and similar print-layouts, though we recommend "
"Scribus for more intensive work."
msgstr ""
"吸附到参考线。参考线可以从标尺上拉出来。隐藏参考线后吸附依然生效。参考线会被"
"保存到文档中。用于对齐漫画画格或者类似的印刷排版。如需进行深入的排版作业，我"
"们推荐使用 Scribus。"

#: ../../user_manual/snapping.rst:40
msgid "Guides"
msgstr "指南"

#: ../../user_manual/snapping.rst:43
msgid ".. image:: images/en/Snap-orthogonal.png"
msgstr ".. image:: images/en/Snap-orthogonal.png"

#: ../../user_manual/snapping.rst:45
msgid ""
"This allows you to snap to a horizontal or vertical line from existing "
"vector objects’s nodes (Unless dealing with resizing the height or width "
"only, in which case you can drag the cursor over the path). This is useful "
"for aligning object horizontally or vertically, like with comic panels."
msgstr ""
"吸附到矢量对象的节点所在的水平或者垂直线 (仅改变宽度和高度的情况不算在内，因"
"为在那种情形下你可以直接把光标拉到与路径重合)。用于在水平或者垂直方向上对齐对"
"象，如漫画画格等。"

#: ../../user_manual/snapping.rst:49
msgid "Orthogonal (Vector Only)"
msgstr "正交线 (仅矢量)"

#: ../../user_manual/snapping.rst:52
msgid ".. image:: images/en/Snap-node.png"
msgstr ".. image:: images/en/Snap-node.png"

#: ../../user_manual/snapping.rst:54
msgid "Node (Vector Only)"
msgstr "节点 (仅矢量)"

#: ../../user_manual/snapping.rst:54
msgid "This snaps a vector node or an object to the nodes of another path."
msgstr "把一个对象或者节点吸附到其他路径的节点上面。"

#: ../../user_manual/snapping.rst:57
msgid ".. image:: images/en/Snap-extension.png"
msgstr ".. image:: images/en/Snap-extension.png"

#: ../../user_manual/snapping.rst:59
msgid ""
"When we draw an open path, the last nodes on either side can be "
"mathematically extended. This option allows you to snap to that. The "
"direction of the node depends on its side handles in path editing mode."
msgstr ""
"如果我们绘制了一根开放的路径，该路径的端点可以按数学原理画出一根延长线。此吸"
"附功能可以吸附到这种延长线上。节点的延长方向取决于在编辑路径模式下的调整点位"
"置。"

#: ../../user_manual/snapping.rst:62
msgid "Extension (Vector Only)"
msgstr "延长线 (仅矢量)"

#: ../../user_manual/snapping.rst:65
msgid ".. image:: images/en/Snap-intersection.png"
msgstr ".. image:: images/en/Snap-intersection.png"

#: ../../user_manual/snapping.rst:66
msgid "Intersection (Vector Only)"
msgstr "交点 (仅矢量)"

#: ../../user_manual/snapping.rst:67
msgid "This allows you to snap to an intersection of two vectors."
msgstr "吸附到两个矢量图形的交点上。"

#: ../../user_manual/snapping.rst:68
msgid "Bounding box (Vector Only)"
msgstr "边框 (仅矢量)"

#: ../../user_manual/snapping.rst:69
msgid "This allows you to snap to the bounding box of a vector shape."
msgstr "吸附到一个矢量形状的外框上。"

#: ../../user_manual/snapping.rst:71
msgid "Image bounds"
msgstr "图像边框"

#: ../../user_manual/snapping.rst:71
msgid "Allows you to snap to the vertical and horizontal borders of an image."
msgstr "吸附到图像的水平和垂直边缘。"

#: ../../user_manual/snapping.rst:74
msgid "Allows you to snap to the horizontal and vertical center of an image."
msgstr "吸附到图像的水平或者垂直中心。"

#: ../../user_manual/snapping.rst:75
msgid "Image center"
msgstr "图像中心"

#: ../../user_manual/snapping.rst:77
msgid "The snap works for the following tools:"
msgstr "吸附功能对下列工具有效："

#: ../../user_manual/snapping.rst:79
msgid "Straight line"
msgstr "直线"

#: ../../user_manual/snapping.rst:80
msgid "Rectangle"
msgstr "矩形"

#: ../../user_manual/snapping.rst:81
msgid "Ellipse"
msgstr "椭圆"

#: ../../user_manual/snapping.rst:82
msgid "Polyline"
msgstr "折线"

#: ../../user_manual/snapping.rst:83
msgid "Path"
msgstr "路径"

#: ../../user_manual/snapping.rst:84
msgid "Freehand path"
msgstr "手绘路径"

#: ../../user_manual/snapping.rst:85
msgid "Polygon"
msgstr "多边形"

#: ../../user_manual/snapping.rst:86
msgid "Gradient"
msgstr "渐变"

#: ../../user_manual/snapping.rst:87
msgid "Shape Handling tool"
msgstr "形状选取工具和形状编辑工具"

#: ../../user_manual/snapping.rst:88
msgid "The Text-tool"
msgstr "文字工具"

#: ../../user_manual/snapping.rst:89
msgid "Assistant editing tools"
msgstr "绘画辅助尺编辑工具"

#: ../../user_manual/snapping.rst:90
msgid ""
"The move tool (note that it snaps to the cursor position and not the "
"bounding box of the layer, selection or whatever you are trying to move)"
msgstr "移动工具 (吸附到光标位置而不是正在移动的图层、选区等对象的外框)"

#: ../../user_manual/snapping.rst:93
msgid "The Transform tool"
msgstr "变形工具"

#: ../../user_manual/snapping.rst:94
msgid "Rectangle select"
msgstr "矩形选区"

#: ../../user_manual/snapping.rst:95
msgid "Elliptical select"
msgstr "椭圆选区"

#: ../../user_manual/snapping.rst:96
msgid "Polygonal select"
msgstr "多边形选区"

#: ../../user_manual/snapping.rst:97
msgid "Path select"
msgstr "路径选区"

#: ../../user_manual/snapping.rst:98
#, fuzzy
#| msgid "Guides themselves can be snapped to grids and vectors."
msgid "Guides themselves can be snapped to grids and vectors"
msgstr "参考线本身也可以吸附到网格和矢量图形上面。"

#: ../../user_manual/snapping.rst:100
msgid ""
"Snapping doesn’t have a sensitivity yet, and by default is set to 10 screen "
"pixels."
msgstr "吸附功能尚未具备敏感度选项，它的默认值为 10 个屏幕像素。"
