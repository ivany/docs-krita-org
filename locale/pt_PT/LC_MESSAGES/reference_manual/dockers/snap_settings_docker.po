# SOME DESCRIPTIVE TITLE.
# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-02-19 03:36+0100\n"
"PO-Revision-Date: 2019-02-21 17:56+0000\n"
"Last-Translator: José Nuno Coelho Pires <zepires@gmail.com>\n"
"Language-Team: Portuguese <kde-i18n-pt@kde.org>\n"
"Language: pt\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-POFile-SpellExtra: en Krita image KritaSnapSettingsDocker images ref\n"

#: ../../<generated>:1
msgid "Guides"
msgstr "Guias"

#: ../../reference_manual/dockers/snap_settings_docker.rst:1
msgid "Overview of the snap settings docker."
msgstr "Introdução área de configuração das definições de ajuste."

#: ../../reference_manual/dockers/snap_settings_docker.rst:16
msgid "Snap Settings"
msgstr "Configuração do Ajuste"

#: ../../reference_manual/dockers/snap_settings_docker.rst:20
msgid ""
"This docker has been removed in Krita 3.0. For more information on how to do "
"this instead, consult the :ref:`snapping page <snapping>`."
msgstr ""
"Esta área foi removida no Krita 3.0. Para mais informações sobre como fazer "
"isto em alternativa, consulte a :ref:`página de ajuste <snapping>`."

#: ../../reference_manual/dockers/snap_settings_docker.rst:23
msgid ".. image:: images/en/Krita_Snap_Settings_Docker.png"
msgstr ".. image:: images/en/Krita_Snap_Settings_Docker.png"

#: ../../reference_manual/dockers/snap_settings_docker.rst:24
msgid ""
"This is docker only applies for Vector Layers. Snapping determines where a "
"vector shape will snap. The little number box is for snapping to a grid."
msgstr ""
"Esta é uma área que só se aplica nas Camadas Vectoriais. O ajuste define "
"para onde é que será ajustada a posição de uma forma vectorial. O pequeno "
"campo numérico serve para o ajuste a uma dada grelha."

#: ../../reference_manual/dockers/snap_settings_docker.rst:26
msgid "Node"
msgstr "Nó"

#: ../../reference_manual/dockers/snap_settings_docker.rst:27
msgid "For snapping to other vector nodes."
msgstr "Para se ajustar a outros nós vectoriais."

#: ../../reference_manual/dockers/snap_settings_docker.rst:28
msgid "Extensions of Line"
msgstr "Extensões de Linhas"

#: ../../reference_manual/dockers/snap_settings_docker.rst:29
msgid ""
"For snapping to a point that could have been part of a line, had it been "
"extended."
msgstr ""
"Para ajustar a um ponto que poderia fazer parte do seguimento de uma linha, "
"caso a mesma fosse estendida."

#: ../../reference_manual/dockers/snap_settings_docker.rst:30
msgid "Bounding Box"
msgstr "Zona Envolvente"

#: ../../reference_manual/dockers/snap_settings_docker.rst:31
msgid "For snapping to the bounding box of a vector shape."
msgstr "Para ajustar à área envolvente de uma dada forma vectorial."

#: ../../reference_manual/dockers/snap_settings_docker.rst:32
msgid "Orthogonal"
msgstr "Ortogonal"

#: ../../reference_manual/dockers/snap_settings_docker.rst:33
msgid "For snapping to only horizontal or vertical lines."
msgstr "Para ajustar apenas às linhas horizontais e verticais."

#: ../../reference_manual/dockers/snap_settings_docker.rst:34
msgid "Intersection"
msgstr "Intersecção"

#: ../../reference_manual/dockers/snap_settings_docker.rst:35
msgid "for snapping to other vector lines."
msgstr "Para se ajustar a outras linhas vectoriais."

#: ../../reference_manual/dockers/snap_settings_docker.rst:37
msgid "Guides don't exist in Krita, therefore this one is useless."
msgstr "As guias não existem no Krita, pelo que esta é inútil."
