# translation of docs_krita_org_reference_manual___dockers___artistic_color_selector.po to Slovak
# Roman Paholik <wizzardsk@gmail.com>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___dockers___artistic_color_selector\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-17 03:39+0200\n"
"PO-Revision-Date: 2019-04-02 09:36+0200\n"
"Last-Translator: Roman Paholik <wizzardsk@gmail.com>\n"
"Language-Team: Slovak <kde-sk@linux.sk>\n"
"Language: sk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"X-Generator: Lokalize 18.12.3\n"
"Plural-Forms: nplurals=3; plural=(n==1) ? 0 : (n>=2 && n<=4) ? 1 : 2;\n"

#: ../../<rst_epilog>:2
msgid ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"
msgstr ""
".. image:: images/icons/Krita_mouse_left.png\n"
"   :alt: mouseleft"

#: ../../<rst_epilog>:4
msgid ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"
msgstr ""
".. image:: images/icons/Krita_mouse_right.png\n"
"   :alt: mouseright"

#: ../../reference_manual/dockers/artistic_color_selector.rst:1
msgid "Overview of the artistic color selector docker."
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:16
msgid "Artist Color Selector Docker"
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:19
msgid ".. image:: images/en/Krita_Artistic_Color_Selector_Docker.png"
msgstr ".. image:: images/en/Krita_Artistic_Color_Selector_Docker.png"

#: ../../reference_manual/dockers/artistic_color_selector.rst:20
msgid ""
"A round selector that tries to give you the tools to select colors ramps "
"efficiently."
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:23
msgid "Preference"
msgstr "Priorita"

#: ../../reference_manual/dockers/artistic_color_selector.rst:25
msgid ""
"Set the color model used by the selector, as well as the amount of segments."
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:28
msgid "Reset"
msgstr "Obnoviť"

#: ../../reference_manual/dockers/artistic_color_selector.rst:30
msgid "Reset the selector to a default stage."
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:33
msgid "Absolute"
msgstr "Absolútne"

#: ../../reference_manual/dockers/artistic_color_selector.rst:35
msgid ""
"This changes the algorithm around so it gives proper values for the gray. "
"Without absolute, it'll use HSV values for gray to the corresponding hue and "
"lightness."
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:38
msgid "Usage"
msgstr "Použitie"

#: ../../reference_manual/dockers/artistic_color_selector.rst:40
msgid ""
"|mouseleft| the swatches to change the foreground color. Use |mouseright| + "
"Drag to shift the alignment of the selector swatches within a specific "
"saturation ring. Use |mouseleft| + Drag to shift the alignment of all "
"swatches."
msgstr ""

#: ../../reference_manual/dockers/artistic_color_selector.rst:43
msgid "This selector does not update on change of foreground color."
msgstr ""
