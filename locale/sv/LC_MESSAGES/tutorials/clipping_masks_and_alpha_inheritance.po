# Copyright (C) licensed under the GNU Free Documentation License 1.3+ unless stated otherwise
# This file is distributed under the same license as the Krita Manual package.
#
# Stefan Asserhäll <stefan.asserhall@bredband.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: Krita Manual 4.1\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-03-28 03:35+0100\n"
"PO-Revision-Date: 2019-03-21 20:40+0100\n"
"Last-Translator: Stefan Asserhäll <stefan.asserhall@bredband.net>\n"
"Language-Team: Swedish <kde-i18n-doc@kde.org>\n"
"Language: sv\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n != 1;\n"
"X-Generator: Lokalize 2.0\n"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/en/clipping-masks/Composition_animation.gif\n"
"   :alt: Animation showing that groups are composed before the rest of "
"composition takes place."
msgstr ""
".. image:: images/en/clipping-masks/Composition_animation.gif\n"
"   :alt: Animering som visar att grupper sätts samman innan resten av "
"sammansättningen sker."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/en/Layer-composite.png\n"
"   :alt: An image showing the way layers composite in Krita"
msgstr ""
".. image:: images/en/Layer-composite.png\n"
"   :alt: En bild som visar hur lager sätts samman i Krita"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/en/Krita-tutorial2-I.1-2.png\n"
"   :alt: An image showing how the alpha inheritance works and affects layers."
msgstr ""
".. image:: images/en/Krita-tutorial2-I.1-2.png\n"
"   :alt: En bild som visar hur alfa-arv fungerar och påverkar lager."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/en/clipping-masks/Tut_Clipping_1.png\n"
"   :alt: an image with line art and a layer for each flat of color"
msgstr ""
".. image:: images/en/clipping-masks/Tut_Clipping_1.png\n"
"   :alt: En bild med linjegrafik och ett lager för varje plan av färg"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/en/clipping-masks/Tut_Clipping_2.png\n"
"   :alt: An image showing how the alpha inheritance works and affects layers."
msgstr ""
".. image:: images/en/clipping-masks/Tut_Clipping_2.png\n"
"   :alt: En bild som visar hur alfa-arv fungerar och påverkar lager."

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/en/clipping-masks/Tut_Clipping_3.png\n"
"   :alt: clipping mask step 3"
msgstr ""
".. image:: images/en/clipping-masks/Tut_Clipping_3.png\n"
"   :alt: beskärningsmask steg 3"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/en/clipping-masks/Tut_Clipping_4.png\n"
"   :alt: clipping mask step 4"
msgstr ""
".. image:: images/en/clipping-masks/Tut_Clipping_4.png\n"
"   :alt: beskärningsmask steg 4"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/en/clipping-masks/Tut_Clipping_5.png\n"
"   :alt: clipping mask step 5"
msgstr ""
".. image:: images/en/clipping-masks/Tut_Clipping_5.png\n"
"   :alt: beskärningsmask steg 5"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/en/clipping-masks/Tut_Clipping_6.png\n"
"   :alt: clipping mask step 6"
msgstr ""
".. image:: images/en/clipping-masks/Tut_Clipping_6.png\n"
"   :alt: beskärningsmask steg 6"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/en/clipping-masks/Tut_Clipping_7.png\n"
"   :alt: clipping mask step 7"
msgstr ""
".. image:: images/en/clipping-masks/Tut_Clipping_7.png\n"
"   :alt: beskärningsmask steg 7"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:None
msgid ""
".. image:: images/en/clipping-masks/Tut_clip_blur.gif\n"
"   :alt: filter layers and alpha inheritance"
msgstr ""
".. image:: images/en/clipping-masks/Tut_clip_blur.gif\n"
"   :alt: filterlager och alfa-arv"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:1
msgid "An introduction for using clipping masks in Krita."
msgstr "En introduktion till användning av beskärningsmasker i Krita"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:15
msgid "Clipping Masks and Alpha Inheritance"
msgstr "Beskärningsmasker och alfa-arv"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:17
msgid ""
"Krita doesn't have clipping mask functionality in the manner that Photoshop "
"and programs that mimic Photoshop's functionality have. That's because in "
"Krita, unlike such software, a group layer is not an arbitrary collection of "
"layers. Rather, in Krita, group layers are composited separately from the "
"rest of the stack, and then the result is added into the stack. In other "
"words, in Krita group layers are in effect a separate image inside your "
"image."
msgstr ""

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:24
msgid ""
"The exception is when using pass-through mode, meaning that alpha "
"inheritance won't work right when turning on pass-through on the layer."
msgstr ""

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:30
msgid ""
"When we turn on alpha inheritance, the alpha-inherited layer keeps the same "
"transparency as the layers below."
msgstr ""

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:36
msgid ""
"Combined with group layers this can be quite powerful. A situation where "
"this is particularly useful is the following:"
msgstr ""
"Kombinerat med grupplager kan det vara riktigt kraftfullt. En situation där "
"det är särskilt användbart är följande:"

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:42
msgid ""
"Here we have an image with line art and a layer for each flat of colors. We "
"want to add complicated multi-layered shading to this, while keeping the "
"neatness of the existing color flats. To get a clipping mask working, you "
"first need to put layers into a group. You can do this by making a group "
"layer and drag-and-dropping the layers into it, or by selecting the layers "
"you want grouped and pressing :kbd:`Ctrl+G`. Here we do that with the iris "
"and the eye-white layers."
msgstr ""

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:50
msgid ""
"We add a layer for the highlight above the other two layers, and add some "
"white scribbles."
msgstr ""

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:60
msgid ""
"In the above, we have our layer with a white scribble on the left, and on "
"the right, the same layer, but with alpha inheritance active, limiting it to "
"the combined area of the iris and eye-white layers."
msgstr ""

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:66
msgid ""
"Now there’s an easier way to set up alpha inheritance. If you select a layer "
"or set of layers and press :kbd:`Ctrl+Shift+G`, you create a quick clipping "
"group. That is, you group the layers, and a ‘mask layer’ set with alpha "
"inheritance is added on top."
msgstr ""

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:76
msgid ""
"The fact that alpha inheritance can use the composited transparency from a "
"combination of layers means that you can have a layer with the erase-"
"blending mode in between, and have that affect the area that the layer above "
"is clipped to. Above, the lower image is exactly the same as the upper one, "
"except with the erase-layer hidden. Filters can also affect the alpha "
"inheritance:"
msgstr ""

#: ../../tutorials/clipping_masks_and_alpha_inheritance.rst:83
msgid ""
"Above, the blur filter layer gives different results when in different "
"places, due to different parts being blurred."
msgstr ""
"Ovan ger suddighetsfilterlagret olika resultat på olika ställen, på grund av "
"att olika delar blir suddiga."
