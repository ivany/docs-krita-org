# Translation of docs_krita_org_reference_manual___blending_modes___hsx.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: docs_krita_org_reference_manual___blending_modes___hsx\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-17 03:39+0200\n"
"PO-Revision-Date: 2019-04-17 07:31+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../reference_manual/blending_modes/hsx.rst:1
msgid ""
"Page about the HSX blending modes in Krita, amongst which Hue, Color, "
"Luminosity and Saturation."
msgstr ""
"Розділ щодо режимів змішування HSX у Krita, зокрема режимів змішування "
"відтінків, кольору, світності та насиченості."

#: ../../reference_manual/blending_modes/hsx.rst:15
msgid "HSX"
msgstr "HSX"

#: ../../reference_manual/blending_modes/hsx.rst:17
msgid ""
"Krita has four different HSX coordinate systems. The difference between them "
"is how they handle tone."
msgstr ""
"У Krita передбачено підтримку чотирьох різних координатних систем HSX. "
"Відмінності між ними полягають у способі обробці даних щодо тону кольору."

#: ../../reference_manual/blending_modes/hsx.rst:20
msgid "HSI"
msgstr "HSI"

#: ../../reference_manual/blending_modes/hsx.rst:22
msgid ""
"HSI is a color coordinate system, using Hue, Saturation and Intensity to "
"categorize a color. Hue is roughly the wavelength, whether the color is red, "
"yellow, green, cyan, blue or purple. It is measure in 360°, with 0 being "
"red. Saturation is the measurement of how close a color is to gray. "
"Intensity, in this case is the tone of the color. What makes intensity "
"special is that it recognizes yellow (rgb:1,1,0) having a higher combined "
"rgb value than blue (rgb:0,0,1). This is a non-linear tone dimension, which "
"means it's gamma-corrected."
msgstr ""
"HSI — координатна система кольорів, у якій для категоризації кольору "
"використовуються відтінок (Hue), насиченість (Saturation) та інтенсивність "
"(Intensity). Відтінок можна грубо пов'язати із довжиною хвилі, яка визначає, "
"яким є колір: червоним, зеленим, блакитним, синім чи пурпуровим. Це значення "
"вимірюється як кут; максимальним є кут 360°, 0 відповідає червоному кольору. "
"Насиченість — міра того, наскільки колір близький до сірого. Інтенсивність у "
"цьому випадку відповідає за тон кольору. Особливою інтенсивність робить те, "
"що за нею жовтий (rgb:1,1,0) має вище комбіноване значення rgb за синій "
"(rgb:0,0,1). Це нелінійний вимір тону, що означає, що він є тоном із "
"виправленням гами."

#: ../../reference_manual/blending_modes/hsx.rst:28
msgid "HSL"
msgstr "HSL"

#: ../../reference_manual/blending_modes/hsx.rst:30
msgid ""
"HSL is also a color coordinate system. It describes colors in Hue, "
"Saturation and Lightness. Lightness specifically puts both yellow "
"(rgb:1,1,0), blue (rgb:0,0,1) and middle gray (rgb:0.5,0.5,0.5) at the same "
"lightness (0.5)."
msgstr ""
"HSL — також координатна система кольорів. У ній колір описується за "
"допомогою відтінку (Hue), насиченості (Saturation) та освітленості "
"(Lightness). Освітленість для жовтого (rgb:1,1,0), синього (rgb:0,0,1) і "
"помірно сірого (rgb:0.5,0.5,0.5) є однаковою (0.5)."

#: ../../reference_manual/blending_modes/hsx.rst:34
msgid "HSV"
msgstr "HSV"

#: ../../reference_manual/blending_modes/hsx.rst:36
msgid ""
"HSV, occasionally called HSB, is a color coordinate system. It measures "
"colors in Hue, Saturation, and Value (also called Brightness). Value or "
"Brightness specifically refers to strength at which the pixel-lights on your "
"monitor have to shine. It sets Yellow (rgb:1,1,0), Blue (rgb:0,0,1) and "
"White (rgb:1,1,0) at the same Value (100%)"
msgstr ""
"HSV, який іноді називають HSB, є координатною системою кольорів. Тут кольори "
"вимірюються за відтінком (Hue), насиченість (Saturation) і значенням "
"(Value), яке також називають яскравістю (Brightness). Значення або "
"яскравість пов'язані зі світністю пікселя монітора, який показує відповідний "
"колір. Значення для жовтого (rgb:1,1,0), синього (rgb:0,0,1) і білого "
"(rgb:1,1,0) є однаковим (100%)"

#: ../../reference_manual/blending_modes/hsx.rst:40
msgid "HSY"
msgstr "HSY"

#: ../../reference_manual/blending_modes/hsx.rst:42
msgid ""
"HSY is a color coordinate system. It categorizes colors in Hue, Saturation "
"and Luminosity. Well, not really, it uses Luma instead of true luminosity, "
"the difference being that Luminosity is linear while Luma is gamma-corrected "
"and just weights the rgb components. Luma is based on scientific studies of "
"how much light a color reflects in real-life. While like intensity it "
"acknowledges that yellow (rgb:1,1,0) is lighter than blue (rgb:0,0,1), it "
"also acknowledges that yellow (rgb:1,1,0) is lighter than cyan (rgb:0,1,1), "
"based on these studies."
msgstr ""
"HSY є координатною системою кольорів. У ній колір категоризується за "
"відтінком (Hue), насиченістю (Saturation) та світністю (Luminosity). Гаразд, "
"не зовсім так: замість справжньої світності використовується яскравість. "
"Відмінність полягає у тому, що світність є лінійною, а яскравість є "
"значенням із виправленням гами, яке зважено оцінює компоненти RGB. Формула "
"визначення яскравості є наслідком наукових досліджень щодо того, яку частку "
"світла відбиває певний колір у реальному світі. Хоча, як і інтенсивність, "
"цей параметр визначає, що жовтий (rgb:1,1,0) є світлішим за синій "
"(rgb:0,0,1), він, на основі досліджень, також надає змогу стверджувати, що "
"жовтий (rgb:1,1,0) є світлішим за блакитний (rgb:0,1,1)."

#: ../../reference_manual/blending_modes/hsx.rst:46
msgid "HSX Blending Modes"
msgstr "Режими змішування HSX"

#: ../../reference_manual/blending_modes/hsx.rst:55
msgid "Color, HSV, HSI, HSL, HSY"
msgstr "Колір, HSV, HSI, HSL, HSY"

#: ../../reference_manual/blending_modes/hsx.rst:57
msgid ""
"This takes the Luminosity/Value/Intensity/Lightness of the colors on the "
"lower layer, and combines them with the Saturation and Hue of the upper "
"pixels. We refer to Color HSY as 'Color' in line with other applications."
msgstr ""
"У цьому способі світність, значення, інтенсивність чи освітленість кольорів "
"нижнього шару поєднується із насиченістю та відтінком кольорів верхнього "
"шару. У нашій програмі режим «Кольоровий HSY» називається просто «Колір», як "
"це робиться в інших програмах."

#: ../../reference_manual/blending_modes/hsx.rst:62
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSI_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSI_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/hsx.rst:62
#: ../../reference_manual/blending_modes/hsx.rst:67
#: ../../reference_manual/blending_modes/hsx.rst:72
msgid "Left: **Normal**. Right: **Color HSI**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **HSI**."

#: ../../reference_manual/blending_modes/hsx.rst:67
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSI_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSI_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/hsx.rst:72
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:78
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:78
msgid "Left: **Normal**. Right: **Color HSL**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **HSL**."

#: ../../reference_manual/blending_modes/hsx.rst:84
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Color_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:84
msgid "Left: **Normal**. Right: **Color HSV**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **HSV**."

#: ../../reference_manual/blending_modes/hsx.rst:90
msgid ""
".. image:: images/blending_modes/Blending_modes_Color_Sample_image_with_dots."
"png"
msgstr ""
".. image:: images/blending_modes/Blending_modes_Color_Sample_image_with_dots."
"png"

#: ../../reference_manual/blending_modes/hsx.rst:90
msgid "Left: **Normal**. Right: **Color**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Колір**."

#: ../../reference_manual/blending_modes/hsx.rst:99
msgid "Hue HSV, HSI, HSL, HSY"
msgstr "Відтінок HSV, HSI, HSL, HSY"

#: ../../reference_manual/blending_modes/hsx.rst:101
msgid ""
"Takes the saturation and tone of the lower layer and combines them with the "
"hue of the upper-layer. Tone in this case being either Value, Lightness, "
"Intensity or Luminosity."
msgstr ""
"У цьому режимі насиченість і тон нижнього шару поєднуються із відтінком "
"верхнього шару. Тоном у цьому випадку вважається значення, освітленість, "
"інтенсивність або світність."

#: ../../reference_manual/blending_modes/hsx.rst:107
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Hue_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Hue_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:107
msgid "Left: **Normal**. Right: **Hue HSI**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Відтінок HSI**."

#: ../../reference_manual/blending_modes/hsx.rst:113
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Hue_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Hue_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:113
msgid "Left: **Normal**. Right: **Hue HSL**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Відтінок HSL**."

#: ../../reference_manual/blending_modes/hsx.rst:119
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Hue_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Hue_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:119
msgid "Left: **Normal**. Right: **Hue HSV**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Відтінок HSV**."

#: ../../reference_manual/blending_modes/hsx.rst:125
msgid ""
".. image:: images/blending_modes/Blending_modes_Hue_Sample_image_with_dots."
"png"
msgstr ""
".. image:: images/blending_modes/Blending_modes_Hue_Sample_image_with_dots."
"png"

#: ../../reference_manual/blending_modes/hsx.rst:125
msgid "Left: **Normal**. Right: **Hue**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Відтінок**."

#: ../../reference_manual/blending_modes/hsx.rst:134
msgid "Increase Value, Lightness, Intensity or Luminosity."
msgstr "Збільшення значення, освітленості, інтенсивності та світності."

#: ../../reference_manual/blending_modes/hsx.rst:136
msgid ""
"Similar to lighten, but specific to tone. Checks whether the upper layer's "
"pixel has a higher tone than the lower layer's pixel. If so, the tone is "
"increased, if not, the lower layer's tone is maintained."
msgstr ""
"Подібний до «Світліше», але пов'язаний лише з тоном. Виконується перевірка, "
"чи є тон пікселя верхнього шару вищим за тон пікселя нижнього шару. Якщо це "
"так, тон збільшується. Якщо ні, зберігається тон нижнього шару."

#: ../../reference_manual/blending_modes/hsx.rst:142
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Intensity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Intensity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:142
msgid "Left: **Normal**. Right: **Increase Intensity**."
msgstr ""
"Ліворуч: **звичайне змішування**. Праворуч: **Збільшення інтенсивності**."

#: ../../reference_manual/blending_modes/hsx.rst:148
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Lightness_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Lightness_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:148
msgid "Left: **Normal**. Right: **Increase Lightness**."
msgstr ""
"Ліворуч: **звичайне змішування**. Праворуч: **Збільшення освітленості**."

#: ../../reference_manual/blending_modes/hsx.rst:154
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Value_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Value_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:154
msgid "Left: **Normal**. Right: **Increase Value**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Збільшення значення**."

#: ../../reference_manual/blending_modes/hsx.rst:160
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Luminosity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Luminosity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:160
msgid "Left: **Normal**. Right: **Increase Luminosity**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Збільшення світності**."

#: ../../reference_manual/blending_modes/hsx.rst:170
msgid "Increase Saturation HSI, HSV, HSL, HSY"
msgstr "Збільшення насиченості HSI, HSV, HSL, HSY"

#: ../../reference_manual/blending_modes/hsx.rst:172
msgid ""
"Similar to lighten, but specific to Saturation. Checks whether the upper "
"layer's pixel has a higher Saturation than the lower layer's pixel. If so, "
"the Saturation is increased, if not, the lower layer's Saturation is "
"maintained."
msgstr ""
"Подібний до «Світліше», але пов'язаний лише з насиченістю. Виконується "
"перевірка, чи є насиченість пікселя верхнього шару вищою за насиченість "
"пікселя нижнього шару. Якщо це так, насиченість збільшується. Якщо ні, "
"зберігається насиченість нижнього шару."

#: ../../reference_manual/blending_modes/hsx.rst:178
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Saturation_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Saturation_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:178
msgid "Left: **Normal**. Right: **Increase Saturation HSI**."
msgstr ""
"Ліворуч: **звичайне змішування**. Праворуч: **Збільшення насиченості HSI**."

#: ../../reference_manual/blending_modes/hsx.rst:184
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Saturation_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Saturation_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:184
msgid "Left: **Normal**. Right: **Increase Saturation HSL**."
msgstr ""
"Ліворуч: **звичайне змішування**. Праворуч: **Збільшення насиченості HSL**."

#: ../../reference_manual/blending_modes/hsx.rst:190
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Saturation_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Saturation_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:190
msgid "Left: **Normal**. Right: **Increase Saturation HSV**."
msgstr ""
"Ліворуч: **звичайне змішування**. Праворуч: **Збільшення насиченості HSV**."

#: ../../reference_manual/blending_modes/hsx.rst:196
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Saturation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Increase_Saturation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:196
msgid "Left: **Normal**. Right: **Increase Saturation**."
msgstr ""
"Ліворуч: **звичайне змішування**. Праворуч: **Збільшення насиченості**."

#: ../../reference_manual/blending_modes/hsx.rst:202
msgid "Intensity"
msgstr "Інтенсивність"

#: ../../reference_manual/blending_modes/hsx.rst:204
msgid ""
"Takes the Hue and Saturation of the Lower layer and outputs them with the "
"intensity of the upper layer."
msgstr ""
"Використовує значення відтінку і насиченості для нижчого шару і виводить їх "
"зі значенням інтенсивності вищого шару."

#: ../../reference_manual/blending_modes/hsx.rst:209
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Intensity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Intensity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:209
msgid "Left: **Normal**. Right: **Intensity**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Інтенсивність**."

#: ../../reference_manual/blending_modes/hsx.rst:214
msgid "Value"
msgstr "Значення"

#: ../../reference_manual/blending_modes/hsx.rst:216
msgid ""
"Takes the Hue and Saturation of the Lower layer and outputs them with the "
"Value of the upper layer."
msgstr ""
"Використовує значення відтінку і насиченості для нижчого шару і виводить їх "
"із використанням значення вищого шару."

#: ../../reference_manual/blending_modes/hsx.rst:221
msgid ""
".. image:: images/blending_modes/Blending_modes_Value_Sample_image_with_dots."
"png"
msgstr ""
".. image:: images/blending_modes/Blending_modes_Value_Sample_image_with_dots."
"png"

#: ../../reference_manual/blending_modes/hsx.rst:221
msgid "Left: **Normal**. Right: **Value**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Значення**."

#: ../../reference_manual/blending_modes/hsx.rst:226
msgid "Lightness"
msgstr "Освітленість"

#: ../../reference_manual/blending_modes/hsx.rst:228
msgid ""
"Takes the Hue and Saturation of the Lower layer and outputs them with the "
"Lightness of the upper layer."
msgstr ""
"Використовує значення відтінку і насиченості для нижчого шару і виводить їх "
"зі значенням освітленості вищого шару."

#: ../../reference_manual/blending_modes/hsx.rst:233
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Lightness_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Lightness_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:233
msgid "Left: **Normal**. Right: **Lightness**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Освітленість**."

#: ../../reference_manual/blending_modes/hsx.rst:238
msgid "Luminosity"
msgstr "Яскравість"

#: ../../reference_manual/blending_modes/hsx.rst:240
msgid ""
"As explained above, actually Luma, but called this way as it's in line with "
"the terminology in other applications. Takes the Hue and Saturation of the "
"Lower layer and outputs them with the Luminosity of the upper layer. The "
"most preferred one of the four Tone blending modes, as this one gives fairly "
"intuitive results for the Tone of a hue"
msgstr ""
"Як ми вже пояснювали вище, це насправді яскравість, але ми її так називаємо, "
"щоб зберігати сумісність термінології із іншими програмами. У цьому режимі "
"програма використовує відтінок і насиченість нижнього шару і виводить їх зі "
"світністю верхнього шару. Цьому режиму змішування варто надавати перевагу "
"над іншими чотирма режимами змішування тону, оскільки від дає "
"найпрогнозованіші результати для тону відтінку."

#: ../../reference_manual/blending_modes/hsx.rst:247
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Luminosity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Luminosity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:247
msgid "Left: **Normal**. Right: **Luminosity**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Світність**."

#: ../../reference_manual/blending_modes/hsx.rst:256
msgid "Saturation HSI, HSV, HSL, HSY"
msgstr "Насичення HSI, HSV, HSL, HSY"

#: ../../reference_manual/blending_modes/hsx.rst:258
msgid ""
"Takes the Intensity and Hue of the lower layer, and outputs them with the "
"HSI saturation of the upper layer."
msgstr ""
"У цьому режимі програма використовує інтенсивність і відтінок нижнього шару "
"і виводить їх із насиченістю HSI верхнього шару."

#: ../../reference_manual/blending_modes/hsx.rst:263
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Saturation_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Saturation_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:263
msgid "Left: **Normal**. Right: **Saturation HSI**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Насиченість HSI**."

#: ../../reference_manual/blending_modes/hsx.rst:269
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Saturation_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Saturation_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:269
msgid "Left: **Normal**. Right: **Saturation HSL**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Насиченість HSL**."

#: ../../reference_manual/blending_modes/hsx.rst:275
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Saturation_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Saturation_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:275
msgid "Left: **Normal**. Right: **Saturation HSV**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Насиченість HSV**."

#: ../../reference_manual/blending_modes/hsx.rst:281
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Saturation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Saturation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:281
msgid "Left: **Normal**. Right: **Saturation**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Насиченість**."

#: ../../reference_manual/blending_modes/hsx.rst:289
msgid "Decrease Value, Lightness, Intensity or Luminosity"
msgstr "Зменшення значення, освітленості, інтенсивності та світності"

#: ../../reference_manual/blending_modes/hsx.rst:291
msgid ""
"Similar to darken, but specific to tone. Checks whether the upper layer's "
"pixel has a lower tone than the lower layer's pixel. If so, the tone is "
"decreased, if not, the lower layer's tone is maintained."
msgstr ""
"Подібний до «Темніше», але пов'язаний лише з тоном. Виконується перевірка, "
"чи є тон пікселя верхнього шару нижчим за тон пікселя нижнього шару. Якщо це "
"так, тон зменшується. Якщо ні, зберігається тон нижнього шару."

#: ../../reference_manual/blending_modes/hsx.rst:297
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Intensity_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Intensity_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/hsx.rst:297
#: ../../reference_manual/blending_modes/hsx.rst:302
#: ../../reference_manual/blending_modes/hsx.rst:307
msgid "Left: **Normal**. Right: **Decrease Intensity**."
msgstr ""
"Ліворуч: **звичайне змішування**. Праворуч: **Зменшення інтенсивності**."

#: ../../reference_manual/blending_modes/hsx.rst:302
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Intensity_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Intensity_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/hsx.rst:307
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Intensity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Intensity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:313
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Lightness_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Lightness_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:313
msgid "Left: **Normal**. Right: **Decrease Lightness**."
msgstr ""
"Ліворуч: **звичайне змішування**. Праворуч: **Зменшення освітленості**."

#: ../../reference_manual/blending_modes/hsx.rst:319
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Value_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Value_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:319
msgid "Left: **Normal**. Right: **Decrease Value**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Зменшення значення**."

#: ../../reference_manual/blending_modes/hsx.rst:325
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Luminosity_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Luminosity_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:325
msgid "Left: **Normal**. Right: **Decrease Luminosity**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Зменшення світності**."

#: ../../reference_manual/blending_modes/hsx.rst:334
msgid "Decrease Saturation HSI, HSV, HSL, HSY"
msgstr "Зменшення насиченості HSI, HSV, HSL, HSY"

#: ../../reference_manual/blending_modes/hsx.rst:336
msgid ""
"Similar to darken, but specific to Saturation. Checks whether the upper "
"layer's pixel has a lower Saturation than the lower layer's pixel. If so, "
"the Saturation is decreased, if not, the lower layer's Saturation is "
"maintained."
msgstr ""
"Подібний до «Темніше», але пов'язаний лише з насиченістю. Виконується "
"перевірка, чи є насиченість пікселя верхнього шару нижчою за насиченість "
"пікселя нижнього шару. Якщо це так, насиченість зменшується. Якщо ні, "
"зберігається насиченість нижнього шару."

#: ../../reference_manual/blending_modes/hsx.rst:342
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSI_Gray_0.4_and_Gray_0.5.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSI_Gray_0.4_and_Gray_0.5.png"

#: ../../reference_manual/blending_modes/hsx.rst:342
#: ../../reference_manual/blending_modes/hsx.rst:347
#: ../../reference_manual/blending_modes/hsx.rst:352
msgid "Left: **Normal**. Right: **Decrease Saturation HSI**."
msgstr ""
"Ліворуч: **звичайне змішування**. Праворуч: **Зменшення насиченості HSI**."

#: ../../reference_manual/blending_modes/hsx.rst:347
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSI_Light_blue_and_Orange.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSI_Light_blue_and_Orange.png"

#: ../../reference_manual/blending_modes/hsx.rst:352
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSI_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSI_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:358
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSL_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSL_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:358
msgid "Left: **Normal**. Right: **Decrease Saturation HSL**."
msgstr ""
"Ліворуч: **звичайне змішування**. Праворуч: **Зменшення насиченості HSL**."

#: ../../reference_manual/blending_modes/hsx.rst:364
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSV_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_HSV_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:364
msgid "Left: **Normal**. Right: **Decrease Saturation HSV**."
msgstr ""
"Ліворуч: **звичайне змішування**. Праворуч: **Зменшення насиченості HSV**."

#: ../../reference_manual/blending_modes/hsx.rst:370
msgid ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_Sample_image_with_dots.png"
msgstr ""
".. image:: images/blending_modes/"
"Blending_modes_Decrease_Saturation_Sample_image_with_dots.png"

#: ../../reference_manual/blending_modes/hsx.rst:370
msgid "Left: **Normal**. Right: **Decrease Saturation**."
msgstr "Ліворуч: **звичайне змішування**. Праворуч: **Зменшення насиченості**."
