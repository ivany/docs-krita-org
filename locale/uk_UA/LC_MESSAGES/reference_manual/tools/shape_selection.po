# Translation of docs_krita_org_reference_manual___tools___shape_selection.po to Ukrainian
# Copyright (C) 2019 This_file_is_part_of_KDE
# This file is distributed under the license LGPL version 2.1 or
# version 3 or later versions approved by the membership of KDE e.V.
#
# Yuri Chornoivan <yurchor@ukr.net>, 2019.
msgid ""
msgstr ""
"Project-Id-Version: "
"docs_krita_org_reference_manual___tools___shape_selection\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2019-04-04 03:36+0200\n"
"PO-Revision-Date: 2019-04-04 07:37+0300\n"
"Last-Translator: Yuri Chornoivan <yurchor@ukr.net>\n"
"Language-Team: Ukrainian <kde-i18n-uk@kde.org>\n"
"Language: uk\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=4; plural=n==1 ? 3 : n%10==1 && n%100!=11 ? 0 : n"
"%10>=2 && n%10<=4 && (n%100<10 || n%100>=20) ? 1 : 2;\n"
"X-Generator: Lokalize 19.03.70\n"

#: ../../<rst_epilog>:10
msgid ""
".. image:: images/icons/shape_select_tool.svg\n"
"   :alt: toolshapeselection"
msgstr ""
".. image:: images/icons/shape_select_tool.svg\n"
"   :alt: toolshapeselection"

#: ../../reference_manual/tools/shape_selection.rst:1
msgid "Krita's shape selection tool reference."
msgstr "Довідник із інструмента позначення форми Krita."

#: ../../reference_manual/tools/shape_selection.rst:17
msgid "Shape Selection Tool"
msgstr "Інструмент «Позначення форми»"

#: ../../reference_manual/tools/shape_selection.rst:19
msgid "|toolshapeselection|"
msgstr "|toolshapeselection|"

#: ../../reference_manual/tools/shape_selection.rst:21
msgid ""
"The shape selection tool used to be called the \"default\" tool. This had to "
"do with Krita being part of an office suite once upon a time. But this is no "
"longer the case, so we renamed it to its purpose in Krita: Selecting shapes! "
"This tool only works on vector layers, so trying to use it on a paint layer "
"will give a notification."
msgstr ""
"Інструмент позначення форм раніше називався «типовим» інструментом. Причина "
"полягала у тому, що колись Krita була частиною великого офісного комплекту "
"програм. Ті часи давно минули, отже ми перейменували його відповідно до "
"призначення у Krita: позначення форм! Цей інструмент працює лише з "
"векторними шарами. Якщо ви спробуєте скористатися ним для шару малювання, "
"програма попередить вас про це."

#: ../../reference_manual/tools/shape_selection.rst:23
msgid ""
"After you create vector shapes, you can use this tool to select, transform, "
"and access the shape's options in the tool options docker. There are a lot "
"of different properties and things you can do with each vector shape."
msgstr ""
"Після створення векторних форм, ви можете скористатися цим інструментом для "
"позначення, перетворення та доступу до параметрів форм за допомогою бічної "
"панелі параметрів інструмента. У програмі передбачено багато різних "
"властивостей та перетворень, які ви можете виконати над будь-якою векторною "
"формою."

#: ../../reference_manual/tools/shape_selection.rst:26
msgid "Selection"
msgstr "Вибрані"

#: ../../reference_manual/tools/shape_selection.rst:28
msgid ""
"Selecting shapes works as follows. You can click on a shape to select a "
"single shape. You can select multiple shapes with click drag."
msgstr ""
"Вибрати форми можна так: ви можете клацнути на формі, щоб позначити окрему "
"форму; ви можете позначити декілька форм натисканням кнопки миші із "
"перетягуванням."

#: ../../reference_manual/tools/shape_selection.rst:30
msgid ""
"There's two types of drag action, a *blue* one and a *green* one. The blue "
"one is created by dragging from left to right, and will only select shapes "
"that are fully covered by it. The green selection is created from right to "
"left and will select all shapes it touches."
msgstr ""
"Існує два типи дій з перетягування, *синя* і *зелена*. Синя створюється "
"перетягуванням зліва праворуч і позначає лише форми, які буде повністю "
"вкрито позначеним прямокутником. Зелене позначення створюють перетягуванням "
"справа ліворуч. Воно позначає усі форми, хоч частина яких потрапила до "
"прямокутника позначення."

#: ../../reference_manual/tools/shape_selection.rst:33
msgid "Rotating, Moving, Scaling, Skewing"
msgstr "Обертання, пересування, масштабування та перекошування"

#: ../../reference_manual/tools/shape_selection.rst:35
msgid ""
"Once an object is selected, a dashed box will appear around the object. "
"There are handles that you can pull and stretch the box to scale it. If you "
"move your cursor just outside the corner handles you can rotate the object."
msgstr ""
"Після позначення об'єкта навколо нього буде намальовано пунктирну рамку. На "
"рамці буде показано точки керування, за допомогою перетягування яких ви "
"зможете стискати або розтягувати об'єкт. Якщо ви наведете вказівник на "
"зовнішню частину кутових точок керування, ви зможете обертати об'єкт."

#: ../../reference_manual/tools/shape_selection.rst:38
msgid "Tool Options"
msgstr "Параметри інструмента"

#: ../../reference_manual/tools/shape_selection.rst:41
msgid ".. image:: images/en/Vector-tool-options.png"
msgstr ".. image:: images/en/Vector-tool-options.png"

#: ../../reference_manual/tools/shape_selection.rst:42
msgid ""
"The tool options of this menu are quite involved, and separated over 3 tabs."
msgstr ""
"Параметри інструмента з цього меню є доволі очевидними, їх поділено між 3 "
"вкладками."

#: ../../reference_manual/tools/shape_selection.rst:45
msgid "Geometry"
msgstr "Геометрія"

#: ../../reference_manual/tools/shape_selection.rst:47
msgid ""
"Geometry is the first section. It allows you to precisely set the x, y, "
"width and height."
msgstr ""
"Перший розділ присвячено геометрії. За допомогою ви можете точно визначити "
"x, y, ширину і висоту."

#: ../../reference_manual/tools/shape_selection.rst:49
msgid "Anchor Lock"
msgstr "Точка прив’язки"

#: ../../reference_manual/tools/shape_selection.rst:50
msgid "This is not implemented at the moment."
msgstr "У поточній версії не реалізовано."

#: ../../reference_manual/tools/shape_selection.rst:51
msgid "Uniform scaling"
msgstr "Однорідне масштабування"

#: ../../reference_manual/tools/shape_selection.rst:52
msgid ""
"When enabled, it will scale the stroke width with the shape, when not "
"enabled, the stroke with will stay the same."
msgstr ""
"Якщо позначено, товщина мазка масштабуватиметься за формою. Якщо не "
"позначено, товщина лишатиметься сталою."

#: ../../reference_manual/tools/shape_selection.rst:53
msgid "Global coordinates"
msgstr "Загальні координати"

#: ../../reference_manual/tools/shape_selection.rst:54
msgid ""
"Determines whether the width and height bars use the width and height of the "
"object, while taking transforms into account."
msgstr ""
"Визначає, чи використовуватиметься для ширини і висоти ширина і висота "
"об'єкта із врахуванням перетворень."

#: ../../reference_manual/tools/shape_selection.rst:56
msgid "Opacity"
msgstr "Непрозорість"

#: ../../reference_manual/tools/shape_selection.rst:56
msgid "The general opacity, or transparency, of the object."
msgstr "Загальна непрозорість або прозорість об'єкта."

#: ../../reference_manual/tools/shape_selection.rst:59
msgid "Stroke"
msgstr "Штрих"

#: ../../reference_manual/tools/shape_selection.rst:61
msgid "The stroke tab determines how stroke around the object should look."
msgstr ""
"Вкладка «Штрих» визначає, як має виглядати штриховий контур навколо об'єкта."

#: ../../reference_manual/tools/shape_selection.rst:63
msgid ""
"The first set of buttons to choose is the fill of the stroke. This has the "
"same features as the fill of the shape, so scroll down a bit for details on "
"that."
msgstr ""
"За допомогою першого набору кнопок можна визначити заповнення штриха. Тут "
"можна визначити ті самі параметри, що і для заповнення форми, — пояснення "
"наведено трохи нижче."

#: ../../reference_manual/tools/shape_selection.rst:65
msgid "Then, there's the settings for the stroke style."
msgstr "Далі, передбачено параметри для стилю штриха."

#: ../../reference_manual/tools/shape_selection.rst:67
msgid "Thickness"
msgstr "Товщина"

#: ../../reference_manual/tools/shape_selection.rst:68
msgid ""
"The width of the stroke is determined by this entry. When creating a shape, "
"Krita will use the current brush size to determine the width of the stroke."
msgstr ""
"За допомогою цього пункту визначається товщина штриха. При створенні форми "
"Krita використовуватиме розмір поточного пензля для визначення товщини "
"штриха."

#: ../../reference_manual/tools/shape_selection.rst:69
msgid "Cap and corner style"
msgstr "Стиль кінчику та кутів"

#: ../../reference_manual/tools/shape_selection.rst:70
msgid ""
"If you press the button after the thickness entry, you will be able to set "
"the stroke cap and the stroke corner style."
msgstr ""
"Якщо ви натиснете кнопку за пунктом товщини, ви зможете встановити параметри "
"кінчика і стиль кутів штриха."

#: ../../reference_manual/tools/shape_selection.rst:71
msgid "Line-style"
msgstr "Стиль ліній"

#: ../../reference_manual/tools/shape_selection.rst:72
msgid "Determines whether the stroke is solid or uses dashes and dots."
msgstr ""
"Визначає, буде штрих суцільним чи використовуватимуться штрихи і пунктир."

#: ../../reference_manual/tools/shape_selection.rst:74
msgid "Markers"
msgstr "Позначки"

#: ../../reference_manual/tools/shape_selection.rst:74
msgid ""
"Which markers can be added to the stroke. Markers are little figures that "
"will appear at the start, end or all the nodes in between depending on your "
"configuration."
msgstr ""
"Визначає, які позначки може бути додано до штриха. Позначки — маленькі "
"фігурки, які буде показано на початку, у кінці або в усіх проміжних вузлах "
"штриха, залежно від налаштувань."

#: ../../reference_manual/tools/shape_selection.rst:77
msgid "Fill"
msgstr "Заповнити"

#: ../../reference_manual/tools/shape_selection.rst:79
msgid ""
"The fill of the shape. As this has the same features as the fill of the "
"stroke, this is explained here as well."
msgstr ""
"Заповнення форми. Оскільки для форм передбачено ті самі можливості із "
"заповнення, що і для штрихів, тут ми також наведемо певні пояснення."

#: ../../reference_manual/tools/shape_selection.rst:81
msgid ""
"A fill can be a flat color, a gradient or a pattern. Or it can be nothing "
"(transparent)"
msgstr ""
"Заповнювати можна суцільним кольором, градієнтом або візерунком. Можна і "
"взагалі не заповнювати форму (лишити її прозорою)."

#: ../../reference_manual/tools/shape_selection.rst:83
msgid "None"
msgstr "Жодного"

#: ../../reference_manual/tools/shape_selection.rst:84
msgid "No fill. It's transparent."
msgstr "Без заповнення. Форма є прозорою."

#: ../../reference_manual/tools/shape_selection.rst:85
msgid "Color"
msgstr "Колір"

#: ../../reference_manual/tools/shape_selection.rst:86
msgid "A flat color, you can select a new one by pressing the color button."
msgstr ""
"Суцільний колір. Вибрати колір можна за допомогою натискання кнопки вибору "
"кольору."

#: ../../reference_manual/tools/shape_selection.rst:88
msgid "This one has a few more options."
msgstr "Для цього засобу передбачено ще декілька параметрів."

#: ../../reference_manual/tools/shape_selection.rst:90
msgid "Type"
msgstr "Тип"

#: ../../reference_manual/tools/shape_selection.rst:91
msgid "A linear or radial gradient."
msgstr "Лінійний або радіальних градієнт."

#: ../../reference_manual/tools/shape_selection.rst:92
msgid "Repeat"
msgstr "Повторення"

#: ../../reference_manual/tools/shape_selection.rst:93
msgid "How the gradient repeats itself."
msgstr "Спосіб повторення градієнта."

#: ../../reference_manual/tools/shape_selection.rst:95
msgid "Preset"
msgstr "Шаблон"

#: ../../reference_manual/tools/shape_selection.rst:95
msgid "A quick menu for choosing a base gradient to edit."
msgstr ""
"Меню пришвидшеного доступу для вибору базового градієнта для редагування."

#: ../../reference_manual/tools/shape_selection.rst:97
msgid ""
"You can edit the gradient in two ways. The first one is the actual gradient "
"in the docker that you can manipulate. Vectors always use stop-gradients. "
"The other way to edit gradients is editing their position on canvas."
msgstr ""
"Ви можете редагувати градієнт у два способи. Перший полягає у зміні "
"параметрів самого градієнта за допомогою бічної панелі. Для векторних форм "
"завжди використовують градієнт із контрольними точками. Іншим способом "
"редагування градієнтів є редагування їхнього розташування на полотні."

#: ../../reference_manual/tools/shape_selection.rst:98
msgid "Gradient"
msgstr "Градієнтний"

#: ../../reference_manual/tools/shape_selection.rst:101
msgid "Patterns"
msgstr "Візерунки"

#: ../../reference_manual/tools/shape_selection.rst:101
msgid "Patterns aren't implemented yet."
msgstr "Візерунки ще не реалізовано."

#: ../../reference_manual/tools/shape_selection.rst:104
msgid "Right-click menu"
msgstr "Контекстне меню"

#: ../../reference_manual/tools/shape_selection.rst:106
msgid ""
"The shape selection tool has a nice right click menu that gives you several "
"features. If you have an object selected, you can perform various functions "
"like cutting, copying, or moving the object to the front or back."
msgstr ""
"Інструмент вибору форм має чудове контекстне меню (його можна викликати "
"клацанням правою кнопкою миші), яке дає доступ до декількох додаткових "
"можливостей. Якщо позначено об'єкт, ви можете виконати з ним різні дії, "
"зокрема копіювання, вирізання та пересування об'єкта на передній чи задній "
"план."

#: ../../reference_manual/tools/shape_selection.rst:109
msgid ".. image:: images/en/Vector-right-click-menu.png"
msgstr ".. image:: images/en/Vector-right-click-menu.png"

#: ../../reference_manual/tools/shape_selection.rst:110
msgid ""
"If you have multiple objects selected you can perform \"Logical Operators\" "
"on them, or boolean operations as they are commonly called. It will be the "
"last item on the right-click menu. You can combine, subtract, intersect, or "
"split the objects."
msgstr ""
"Якщо позначено декілька об'єктів, ви можете виконувати над ними логічні або "
"булеві операції. Для цього призначено останній пункт у контекстному меню. Ви "
"можете об'єднувати, віднімати, перетинати та розділяти об'єкти."
